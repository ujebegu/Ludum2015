﻿using System;
using System.Collections;
using UnityEngine;

public class Element : MonoBehaviour {
    public static event Action OnCollided;
    public static event Action<ElementType> OnCollidedWithBob;

    [SerializeField] private ElementType elementType;

    private float fallSpeed;
    private bool isFalling;

    private bool IsFalling {
        set {
            if (isFalling != value) {
                isFalling = value;
                if (isFalling) {
                    StartCoroutine("FallCoroutine");

                    InputManager.OnLeftButtonClicked += InputManagerOnLeftButtonClicked;
                    InputManager.OnRightButtonClicked += InputManagerOnRightButtonClicked;
                } else {
                    StopCoroutine("FallCoroutine");

                    InputManager.OnLeftButtonClicked -= InputManagerOnLeftButtonClicked;
                    InputManager.OnRightButtonClicked -= InputManagerOnRightButtonClicked;
                }
            }
        }
    }

    public ElementType ElementType {
        get { return elementType; }
    }

    public Transform CachedTransform { get; private set; }

    private void Awake() {
        CachedTransform = transform;
    }

    private void OnTriggerEnter2D(Collider2D triggeredCollider) {
        IsFalling = false;

        if (ElementType == ElementType.SevenBonus) {
            GameManager.Instance.ElementsCount--;

            AudioController.Instance.PlaySfx("ATRIJKA");
        } else {
            GameManager.Instance.ElementsCount++;

            AudioController.Instance.PlaySfx("ZHRET");
        }

        EventActionUtil.FireEvent(OnCollided);
        EventActionUtil.FireEvent(OnCollidedWithBob, ElementType);

        Explode explode = UnityPoolManager.Instance.Pop<Explode>(PoolType.Explode);
        if (explode != null) {
            explode.CachedTransform.position = CachedTransform.position;
            explode.Play();
        }

        Destroy(gameObject);
    }

    private bool isFirst = true;

    private void InputManagerOnLeftButtonClicked() {
        if (Container.LeftContainer.AddElement(this)) {
            IsFalling = false;

            EventActionUtil.FireEvent(OnCollided);
        } else if (isFirst) {
            isFirst = false;
            AudioController.Instance.PlaySfx("SMEH", 0.5f);
        }
    }

    private void InputManagerOnRightButtonClicked() {
        if (Container.RightContainer.AddElement(this)) {
            IsFalling = false;

            EventActionUtil.FireEvent(OnCollided);
        } else if (isFirst) {
            isFirst = false;
            AudioController.Instance.PlaySfx("SMEH", 0.5f);
        }
    }

    public void StartFall() {
        fallSpeed = Mathf.Clamp(GameManager.Instance.LevelSettings.elementMinFallSpeed +
                                Spawner.Instance.SpawnedElementsCount*
                                GameManager.Instance.LevelSettings.elementAccumulatedSpeed,
            GameManager.Instance.LevelSettings.elementMinFallSpeed,
            GameManager.Instance.LevelSettings.elementMaxFallSpeed);

        IsFalling = true;
    }

    public void StopFall() {
        IsFalling = false;
    }

    private IEnumerator FallCoroutine() {
        while (isFalling) {
            CachedTransform.position += Vector3.down*fallSpeed*Time.deltaTime;

            yield return null;
        }
    }
}