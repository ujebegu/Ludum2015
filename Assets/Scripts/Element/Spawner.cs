﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : Singleton<Spawner> {
    [SerializeField] private Vector3 startPosition;
    [SerializeField] private List<Element> elements;
    [SerializeField] private Element bonus;
    [SerializeField] [Range(0f, 1f)] private float bonusProbability;
    [SerializeField] private float roundDelay = 1f;

    private bool isSpawning;
    private Element nextElement;

    public int SpawnedElementsCount { get; private set; }

    protected override void Awake() {
        base.Awake();

        startPosition.y *= Screen.height/800f;
    }

    private void OnEnable() {
        Element.OnCollided += ElementOnCollided;
        GameManager.OnScoreChanged += GameManagerOnScoreChanged;
    }

    private void OnDisable() {
        Element.OnCollided -= ElementOnCollided;
        GameManager.OnScoreChanged -= GameManagerOnScoreChanged;
    }

    private void GameManagerOnScoreChanged(int count) {
        if (isSpawning) {
            SpawnedElementsCount = Mathf.Clamp(SpawnedElementsCount - 2, 0, int.MaxValue);
        }
    }

    private void ElementOnCollided() {
        if (isSpawning) {
            SpawnElement();
        }
    }

    public void StartSpawn() {
        SpawnedElementsCount = 0;

        SpawnElement();

        isSpawning = true;
    }

    public void StopSpawn() {
        isSpawning = false;
    }

    private void SpawnElement() {
        if (nextElement != null) {
            nextElement.StartFall();
        } else {
            var elementInstance =
                Instantiate(elements[Random.Range(0, elements.Count)], startPosition, Quaternion.identity) as Element;
            if (elementInstance != null) {
                StartCoroutine(FallAfterdelay(elementInstance));
            }
        }

        Element nextElementToSpawn = Random.value < bonusProbability ? bonus : elements[Random.Range(0, elements.Count)];

        nextElement =
            Instantiate(nextElementToSpawn, startPosition, Quaternion.identity) as Element;

        if (nextElementToSpawn.ElementType != ElementType.SevenBonus) {
            SpawnedElementsCount++;
        }
    }

    private IEnumerator FallAfterdelay(Element element) {
        yield return new WaitForSeconds(roundDelay);

        element.StartFall();
    }
}