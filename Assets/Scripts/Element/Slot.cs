﻿using UnityEngine;

public class Slot : MonoBehaviour {
    private Element attachedElement;
    private Transform cachedTransform;

    private void Awake() {
        cachedTransform = transform;
    }

    public bool IsUsed {
        get { return attachedElement != null; }
    }

    public ElementType ElementType {
        get { return attachedElement == null ? ElementType.None : attachedElement.ElementType; }
    }

    public void AttachElement(Element element) {
        attachedElement = element;
        element.CachedTransform.position = cachedTransform.position;
    }

    public void Clear() {
        if (attachedElement != null) {
            Destroy(attachedElement.gameObject);
            attachedElement = null;

            Explode explode = UnityPoolManager.Instance.Pop<Explode>(PoolType.Explode);
            if (explode != null) {
                explode.CachedTransform.position = cachedTransform.position;
                explode.Play();
            }
        }
    }
}