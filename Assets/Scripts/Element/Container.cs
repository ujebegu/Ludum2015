﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Container : MonoBehaviour {
    public static Container LeftContainer { get; private set; }
    public static Container RightContainer { get; private set; }

    [SerializeField] private List<Slot> slots;
    [SerializeField] private bool removeNearest;

    public bool IsFull { get; private set; }

    private void Awake() {
        if (transform.position.x < 0) {
            LeftContainer = this;
        } else {
            RightContainer = this;
        }
    }

    public void Clear() {
        foreach (var slot in slots) {
            slot.Clear();
        }
    }

    public bool AddElement(Element element) {
        //foreach (var slot in slots) {
        //    if (!slot.IsUsed) {
        //        slot.AttachElement(element);

        //        StartCoroutine(CheckSlots());

        //        return true;
        //    }
        //}

        //foreach (var slot in slots) {
        //    if (slot.ElementType == element.ElementType) {
        //        slot.Clear();
        //        Destroy(element.gameObject);

        //        GameManager.Instance.ElementsCount++;
        //    }
        //}

        //return true;

        if (element.ElementType == ElementType.SevenBonus) {
            foreach (var slot in slots) {
                slot.Clear();
            }

            element.StopFall();
            Destroy(element.gameObject);

            AudioController.Instance.PlaySfx("Zgoranie", 0.2f);
        } else {
            Slot freeSlot = null;
            Slot usedSlot = null;
            for (int i = slots.Count - 1; i >= 0; i--) {
                if (slots[i].IsUsed) {
                    usedSlot = slots[i];
                    break;
                } else {
                    freeSlot = slots[i];
                }
            }

            if (usedSlot != null && usedSlot.ElementType == element.ElementType) {
                usedSlot.Clear();
                element.StopFall();
                Destroy(element.gameObject);

                GameManager.Instance.ElementsCount--;

                AudioController.Instance.PlaySfx("Zgoranie", 0.2f);
            } else if (freeSlot != null) {
                freeSlot.AttachElement(element);
            } else {
                return false;
            }
        }

        return true;
    }

    private IEnumerator CheckSlots() {
        yield return new WaitForEndOfFrame();

        if (removeNearest) {
            for (int i = slots.Count - 1; i > 0; i--) {
                if (slots[i].ElementType != ElementType.None && slots[i].ElementType == slots[i - 1].ElementType) {
                    slots[i].Clear();
                    slots[i - 1].Clear();

                    GameManager.Instance.ElementsCount--;
                }
            }
        } else {
            for (int i = slots.Count - 1; i > 0; i--) {
                for (int j = i - 1; j >= 0; j--) {
                    if (slots[i].ElementType != ElementType.None && slots[i].ElementType == slots[j].ElementType) {
                        slots[i].Clear();
                        slots[j].Clear();

                        GameManager.Instance.ElementsCount--;
                    }
                }
            }
        }
    }
}