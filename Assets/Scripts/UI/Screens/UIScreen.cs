﻿using System;
using UnityEngine;

public class UIScreen : MonoBehaviour {
    #region Variables

    public event Action<UIScreen> OnShow;
    public event Action<UIScreen> OnHide;

    [SerializeField] private UIScreenType screenType;
    [SerializeField] private bool isShownByDefault;
    private bool isShown;

    private bool IsShown {
        set {
            if (isShown != value) {
                isShown = value;

                if (isShown) {
                    gameObject.SetActive(true);

                    Activate();
                } else {
                    Deactivate();

                    gameObject.SetActive(false);
                }
            }
        }
    }

    public UIScreenType ScreenType {
        get { return screenType; }
    }

    public bool IsShownByDefault {
        get { return isShownByDefault; }
    }

    #endregion

    #region Public methods

    public void Show() {
        IsShown = true;
    }

    public void Hide() {
        IsShown = false;
    }

    #endregion

    #region Private methods

    protected virtual void Activate() {
        EventActionUtil.FireEvent(OnShow, this);
    }

    protected virtual void Deactivate() {
        EventActionUtil.FireEvent(OnHide, this);
    }

    #endregion
}