﻿using UnityEngine;
using UnityEngine.UI;

public class UIStartScreen : UIScreen {
    [SerializeField] private Button play;
    [SerializeField] private Button howToPlay;
    [SerializeField] private Button leaderboard;
    [SerializeField] private Button aboutUs;
    [SerializeField] private Button exit;

    private bool isFirstLaunch = true;

    protected override void Activate() {
        base.Activate();

        play.onClick.AddListener(PlayOnClick);
        exit.onClick.AddListener(ExitOnClick);

        if (isFirstLaunch) {
            isFirstLaunch = false;
        } else {
            AudioController.Instance.MuteAmbient();
        }
    }

    protected override void Deactivate() {
        base.Deactivate();

        play.onClick.RemoveListener(PlayOnClick);
        exit.onClick.RemoveListener(ExitOnClick);

        AudioController.Instance.UnMuteAmbient();
    }

    private void PlayOnClick() {
        UIScreenManager.Instance.HideScreen(UIScreenType.Start);
        UIScreenManager.Instance.ShowScreen(UIScreenType.Game);
    }

    private void ExitOnClick() {
#if !UNITY_EDITOR
        Application.Quit();
#endif
    }
}