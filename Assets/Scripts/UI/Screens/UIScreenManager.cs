﻿using System;
using System.Collections.Generic;
using UnityEngine;

public sealed class UIScreenManager : Singleton<UIScreenManager> {
    #region Variables

    public static event Action<UIScreenType> OnShowScreen;
    public static event Action<UIScreenType> OnHideScreen;

    [SerializeField] private UIScreen[] screens;
    private Dictionary<UIScreenType, UIScreen> screensDictionary = new Dictionary<UIScreenType, UIScreen>();

    #endregion

    #region Unity lifecycle

    protected override void Awake() {
        base.Awake();

        Initialize();
    }

    private void OnEnable() {
        SubscribeOnEvents();
    }

    private void OnDisable() {
        UnsubscribeFromEvents();
    }

    private void Start() {
        ShowDefaultScreens();
    }

    #endregion

    #region Event handlers

    private void UIScreenOnShow(UIScreen uiScreen) {
        EventActionUtil.FireEvent(OnShowScreen, uiScreen.ScreenType);
    }

    private void UIScreenOnHide(UIScreen uiScreen) {
        EventActionUtil.FireEvent(OnHideScreen, uiScreen.ScreenType);
    }

    #endregion

    #region Public methods

    public void ShowScreen(UIScreenType uiScreenType) {
        if (screensDictionary.ContainsKey(uiScreenType)) {
            screensDictionary[uiScreenType].Show();
        }
    }

    public void HideScreen(UIScreenType uiScreenType) {
        if (screensDictionary.ContainsKey(uiScreenType)) {
            screensDictionary[uiScreenType].Hide();
        }
    }

    #endregion

    #region Private methods

    private void ShowDefaultScreens() {
        foreach (var uiScreen in screensDictionary.Values) {
            if (uiScreen.IsShownByDefault) {
                uiScreen.Show();
            } else {
                uiScreen.gameObject.SetActive(false);
            }
        }
    }

    private void Initialize() {
        foreach (var uiScreen in screens) {
            if (!screensDictionary.ContainsKey(uiScreen.ScreenType)) {
                screensDictionary.Add(uiScreen.ScreenType, uiScreen);
            }
        }
    }

    private void SubscribeOnEvents() {
        foreach (var uiScreen in screensDictionary.Values) {
            uiScreen.OnShow += UIScreenOnShow;
            uiScreen.OnHide += UIScreenOnHide;
        }
    }

    private void UnsubscribeFromEvents() {
        foreach (var uiScreen in screensDictionary.Values) {
            uiScreen.OnShow -= UIScreenOnShow;
            uiScreen.OnHide -= UIScreenOnHide;
        }
    }

    #endregion
}