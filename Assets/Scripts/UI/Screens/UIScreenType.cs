﻿public enum UIScreenType : byte {
    None = 0,
    Start = 1,
    Game = 2,
    End = 3
}