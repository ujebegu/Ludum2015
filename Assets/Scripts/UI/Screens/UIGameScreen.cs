﻿public class UIGameScreen : UIScreen {
    protected override void Activate() {
        base.Activate();

        GameManager.Instance.StartGame();
    }
}