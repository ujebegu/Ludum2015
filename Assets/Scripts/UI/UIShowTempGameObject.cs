﻿using System.Collections;
using UnityEngine;

public class UIShowTempGameObject : MonoBehaviour {
    [SerializeField] private GameObject goToActivate;

    private bool isFirstLaunch = true;

    private void OnEnable() {
        if (isFirstLaunch) {
            isFirstLaunch = false;
            goToActivate.SetActive(true);

            UIScreenManager.OnHideScreen += UIScreenManagerOnHideScreen;
        }
    }

    private void UIScreenManagerOnHideScreen(UIScreenType screenType) {
        if (screenType == UIScreenType.Start) {
            goToActivate.SetActive(false);
            UIScreenManager.OnHideScreen -= UIScreenManagerOnHideScreen;
        }
    }

    public void Show() {
        if (!goToActivate.activeInHierarchy) {
            goToActivate.SetActive(true);

            StartCoroutine(Hide());
        }
    }

    private IEnumerator Hide() {
        while (true) {
            if (Input.anyKey) {
                goToActivate.SetActive(false);

                yield break;
            }

            yield return null;
        }
    }
}