﻿using UnityEngine;

public class UICheckVolume : MonoBehaviour {
    [SerializeField] private GameObject musicOn;
    [SerializeField] private GameObject musicOff;
    [SerializeField] private GameObject soundsOn;
    [SerializeField] private GameObject soundsOff;

    private void Awake() {
        bool activateMusic = PlayerPrefs.GetFloat("VolumeMusic", 1f) > float.Epsilon;

        musicOff.SetActive(activateMusic);
        musicOn.SetActive(!activateMusic);

        bool activateSfx = PlayerPrefs.GetFloat("VolumeEffect", 1f) > float.Epsilon;

        soundsOff.SetActive(activateSfx);
        soundsOn.SetActive(!activateSfx);
    }
}