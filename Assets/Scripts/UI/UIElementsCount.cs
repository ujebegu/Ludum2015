﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof (Text))]
public class UIElementsCount : MonoBehaviour {
    private Text attachedText;

    private void Awake() {
        attachedText = GetComponent<Text>();
    }

    private void OnEnable() {
        GameManager.OnElementsCountChanged += GameManagerOnElementsCountChanged;
    }

    private void OnDisable() {
        GameManager.OnElementsCountChanged -= GameManagerOnElementsCountChanged;
    }


    private void GameManagerOnElementsCountChanged(int count) {
        attachedText.text = count.ToString();
    }
}