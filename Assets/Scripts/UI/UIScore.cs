﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UIScore : MonoBehaviour {
    private const string PREFIX = "Score \n";

    private Text attachedText;

    private void Awake() {
        attachedText = GetComponent<Text>();
    }

    private void OnEnable() {
        ScoreManager.OnScoreChanged += ScoreManagerOnScoreChanged;

        ScoreManagerOnScoreChanged(ScoreManager.Instance.Score);
    }

    private void OnDisable() {
        ScoreManager.OnScoreChanged -= ScoreManagerOnScoreChanged;
    }

    private void ScoreManagerOnScoreChanged(int count) {
        attachedText.text = PREFIX + count;
    }
}