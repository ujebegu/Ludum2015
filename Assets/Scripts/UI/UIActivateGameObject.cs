﻿using System;
using UnityEngine;

public class UIActivateGameObject : MonoBehaviour {
    [Serializable]
    public struct GOInfo {
        public GameObject gameObject;
        public bool setActive;
    }

    [SerializeField] private GOInfo[] info;

    public void Activate() {
        foreach (var goInfo in info) {
            goInfo.gameObject.SetActive(goInfo.setActive);
        }
    }
}