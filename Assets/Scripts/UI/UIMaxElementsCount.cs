﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UIMaxElementsCount : MonoBehaviour {
    private Text attachedText;

    private void Awake() {
        attachedText = GetComponent<Text>();
    }

    private void OnEnable() {
        GameManager.OnMaxElementsCountChanged += GameManagerOnMaxElementsCountChanged;
    }

    private void OnDisable() {
        GameManager.OnMaxElementsCountChanged -= GameManagerOnMaxElementsCountChanged;
    }


    private void GameManagerOnMaxElementsCountChanged(int count) {
        attachedText.text = count.ToString();
    }
}