﻿using UnityEngine;

public class UIEnableSounds : MonoBehaviour {
    [SerializeField] private string key;
    [SerializeField] private bool enable;

    public void Change() {
        if (key.Contains("Effect")) {
            PlayerPrefs.SetFloat(key, enable ? 1f : 0f);
        } else {
            PlayerPrefs.SetFloat(key, enable ? AudioController.Instance.CurrentAmbientVolume : 0f);
        }
        

        PlayerPrefs.Save();

        AudioController.Instance.UpdateVolume();
    }
}