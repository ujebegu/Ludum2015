﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UIHighscore : MonoBehaviour {
    private const string PREFIX = "Highscore \n";

    private Text attachedText;

    private void Awake() {
        attachedText = GetComponent<Text>();
    }

    private void OnEnable() {
        ScoreManager.OnHighscoreChanged += ScoreManagerOnHighscoreChanged;

        ScoreManagerOnHighscoreChanged(ScoreManager.Instance.Highscore);
    }

    private void OnDisable() {
        ScoreManager.OnHighscoreChanged -= ScoreManagerOnHighscoreChanged;
    }

    private void ScoreManagerOnHighscoreChanged(int count) {
        attachedText.text = PREFIX + count;
    }
}