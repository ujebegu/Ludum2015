﻿using UnityEngine;

public class BobScale : MonoBehaviour {
    [SerializeField] private Vector3 startScale;
    [SerializeField] private Vector3 endScale;
    [SerializeField] private float scaleTime;

    private Transform cachedTransform;

    private void Awake() {
        cachedTransform = transform;
        cachedTransform.localScale = startScale;
    }

    private void OnEnable() {
        GameManager.OnElementsCountChanged += GameManagerOnElementsCountChanged;
    }

    private void OnDisable() {
        GameManager.OnElementsCountChanged -= GameManagerOnElementsCountChanged;
    }

    private void GameManagerOnElementsCountChanged(int count) {
        Vector3 nextScale = Vector3.Lerp(startScale, endScale,
            (float)count / GameManager.Instance.LevelSettings.elementsCountToLose);

        if (nextScale != cachedTransform.localScale) {
            iTween.ScaleTo(gameObject, nextScale, scaleTime);
        }
    }
}