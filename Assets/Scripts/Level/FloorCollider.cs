﻿using UnityEngine;

public class FloorCollider : MonoBehaviour {
    [SerializeField] private Vector3 startPosition;
    [SerializeField] private Vector3 endPosition;

    private Transform cachedTransform;

    private void Awake() {
        cachedTransform = transform;
        cachedTransform.position = startPosition;
    }

    private void OnEnable() {
        GameManager.OnElementsCountChanged += GameManagerOnElementsCountChanged;
    }

    private void OnDisable() {
        GameManager.OnElementsCountChanged -= GameManagerOnElementsCountChanged;
    }

    private void GameManagerOnElementsCountChanged(int count) {
        cachedTransform.position = Vector3.Lerp(startPosition, endPosition,
            (float) count/GameManager.Instance.LevelSettings.elementsCountToLose);
    }
}