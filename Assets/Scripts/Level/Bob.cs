﻿using UnityEngine;

public class Bob : MonoBehaviour {
    private readonly int EatHash = Animator.StringToHash("Eat");
    private readonly int BadHash = Animator.StringToHash("Bad");

    private Animator cachedAnimator;

    private void Awake() {
        cachedAnimator = GetComponent<Animator>();
    }

    private void OnEnable() {
        Element.OnCollidedWithBob += ElementOnCollided;
        GameManager.OnScoreChanged += GameManagerOnScoreChanged;
    }

    private void OnDisable() {
        Element.OnCollidedWithBob -= ElementOnCollided;
        GameManager.OnScoreChanged -= GameManagerOnScoreChanged;
    }

    private void ElementOnCollided(ElementType elementType) {
        if (elementType != ElementType.SevenBonus) {
            cachedAnimator.ResetTrigger(EatHash);
            cachedAnimator.SetTrigger(EatHash);
        } else {
            cachedAnimator.ResetTrigger(BadHash);
            cachedAnimator.SetTrigger(BadHash);
        }
    }

    private void GameManagerOnScoreChanged(int score) {
        if (score > 0) {
            cachedAnimator.ResetTrigger(BadHash);
            cachedAnimator.SetTrigger(BadHash);
        }
    }
}