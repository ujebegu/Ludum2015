﻿using UnityEngine;

public class Layouter : MonoBehaviour {
    public bool useWidth;

    private void Awake() {
        Vector3 curPos = transform.position;
        if (useWidth) {
            curPos.x *= Screen.width/1200f;
        } else {
            curPos.y *= Screen.height / 800f;
        }

        transform.position = curPos;
    }
}