﻿using System.Collections;
using UnityEngine;

public class Explode : UnityPoolObject {

    [SerializeField] private GameObject fx;
    [SerializeField] private float lifetime;

    public void Play() {
        fx.SetActive(true);

        StartCoroutine(Hide());
    }

    private IEnumerator Hide() {
        yield return new WaitForSeconds(lifetime);
        fx.SetActive(false);

        Push();
    }
}