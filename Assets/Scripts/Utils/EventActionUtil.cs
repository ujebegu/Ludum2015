﻿using System;

public static class EventActionUtil {
    public static void FireEvent(Action handler) {
        if (handler != null) {
            handler();
        }
    }

    public static void FireEvent<T>(Action<T> handler, T obj) {
        if (handler != null) {
            handler(obj);
        }
    }

    public static void FireEvent<T, K>(Action<T, K> handler, T obj1, K obj2) {
        if (handler != null) {
            handler(obj1, obj2);
        }
    }
}