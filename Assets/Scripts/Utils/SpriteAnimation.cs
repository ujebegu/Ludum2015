﻿using System.Collections;
using UnityEngine;

public class SpriteAnimation : MonoBehaviour {
    [SerializeField] private GameObject[] clips;
    [SerializeField] private float speed;

    private GameObject prevClip;
    private int index;
    private bool isAnimated;

    private void Awake() {
        foreach (var clip in clips) {
            clip.SetActive(false);
        }

        if (clips.Length > 0) {
            IsAnimated = true;
        }
    } 

    public bool IsAnimated {
        set {
            if (isAnimated != value) {
                isAnimated = value;
                if (isAnimated) {
                    StartCoroutine("Lifecycle");
                } else {
                    StopCoroutine("Lifecycle");
                }
            }
        }
    }

    private IEnumerator Lifecycle() {
        while (isAnimated) {
            if (prevClip != null) {
                prevClip.SetActive(false);
            }

            clips[index].SetActive(true);
            prevClip = clips[index];

            index = (index + 1)%clips.Length;

            yield return new WaitForSeconds(speed);
        }
    }
}