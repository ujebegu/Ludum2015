﻿using UnityEngine;

public class LevelSettings : ScriptableObject {
    public int elementsCountToLose;

    public float elementMinFallSpeed;
    public float elementMaxFallSpeed;
    public float elementAccumulatedSpeed;
}