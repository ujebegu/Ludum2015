﻿using UnityEngine;

public class GameSettings : ScriptableObject {
    public LevelSettings[] levelSettings;

    public LevelSettings LevelSettings {
        get { return levelSettings[0]; }
    }
}