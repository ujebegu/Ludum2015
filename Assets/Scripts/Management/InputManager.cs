﻿using System;
using UnityEngine;

public class InputManager : Singleton<InputManager> {
    public static event Action OnLeftButtonClicked;
    public static event Action OnRightButtonClicked;

    private void Update() {
        if (Input.GetKeyDown(KeyCode.LeftArrow)) {
            EventActionUtil.FireEvent(OnLeftButtonClicked);
        } else if (Input.GetKeyDown(KeyCode.RightArrow)) {
            EventActionUtil.FireEvent(OnRightButtonClicked);
        }
    }
}