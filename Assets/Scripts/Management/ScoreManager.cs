﻿using System;
using UnityEngine;

public class ScoreManager : Singleton<ScoreManager> {
    public static event Action<int> OnScoreChanged;
    public static event Action<int> OnHighscoreChanged;

    private const string HIGH_SCORE_KEY = "HighScore";

    private int score;
    private int highscore;

    public int Score {
        get { return score; }
        set {
            score = value;
            EventActionUtil.FireEvent(OnScoreChanged, score);

            Highscore = score;
        }
    }

    public int Highscore {
        get { return highscore; }
        set {
            if (value > highscore) {
                highscore = value;

                PlayerPrefs.SetInt(HIGH_SCORE_KEY, highscore);
                PlayerPrefs.Save();

                EventActionUtil.FireEvent(OnHighscoreChanged, highscore);
            }
        }
    }

    protected override void Awake() {
        base.Awake();

        Highscore = PlayerPrefs.GetInt(HIGH_SCORE_KEY, 0);
    }
}