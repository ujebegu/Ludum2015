﻿using System;
using System.Collections;
using UnityEngine;

public class GameManager : Singleton<GameManager> {
    public static event Action<int> OnElementsCountChanged;
    public static event Action<int> OnMaxElementsCountChanged;
    public static event Action<int> OnScoreChanged;

    [SerializeField] private GameSettings gameSettings;

    private int elementsCount;
    private int score;

    private int Score {
        get { return score; }
        set {
            score = value;

            ScoreManager.Instance.Score = score;

            EventActionUtil.FireEvent(OnScoreChanged, score);
        }
    }

    public int ElementsCount {
        get { return elementsCount; }
        set {
            if (value != 0 && value < elementsCount) {
                Score++;
            }

            elementsCount = Mathf.Clamp(value, 0, int.MaxValue);

            EventActionUtil.FireEvent(OnElementsCountChanged, elementsCount);

            if (elementsCount >= LevelSettings.elementsCountToLose) {
                Spawner.Instance.StopSpawn();

                UIScreenManager.Instance.HideScreen(UIScreenType.Game);

                StartCoroutine(ShowStartScreen());
            }
        }
    }

    private IEnumerator ShowStartScreen() {
        yield return new WaitForSeconds(1f);

        UIScreenManager.Instance.ShowScreen(UIScreenType.Start);
    }

    public LevelSettings LevelSettings { get; private set; }

    public void StartGame() {
        LevelSettings = gameSettings.LevelSettings;

        ResetGame();

        Spawner.Instance.StartSpawn();
    }

    private void ResetGame() {
        Container.LeftContainer.Clear();
        Container.RightContainer.Clear();

        ElementsCount = 0;
        Score = 0;
        EventActionUtil.FireEvent(OnMaxElementsCountChanged, LevelSettings.elementsCountToLose);
    }
}