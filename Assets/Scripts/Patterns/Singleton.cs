﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Singleton<T> {
    private static T instance;

    public static T Instance {
        get {
            if (instance == null) {
                Debug.LogWarning(string.Format("Singleton call before awake!!! Type of {0}", typeof (T)));

                instance = FindObjectOfType(typeof (T)) as T;
                if (instance != null) {
                    DontDestroyOnLoad(instance.gameObject);
                }
            }

            if (instance == null) {
                var obj = new GameObject(typeof (T).ToString());
                instance = obj.AddComponent(typeof (T)) as T;

                if (instance != null) {
                    DontDestroyOnLoad(instance.gameObject);
                }
            }

            return instance;
        }
    }

    protected virtual void Awake() {
        if (instance == null) {
            instance = (T) this;
            DontDestroyOnLoad(instance.gameObject);
        } else if (instance != (T) this) {
            Destroy(this);
        }
    }

    protected virtual void OnApplicationQuit() {
        instance = null;
        Destroy(this);
    }
}