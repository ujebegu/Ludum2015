﻿using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof (AudioSource))]
public class SfxAudioSource : MonoBehaviour {
    #region Variables

    public static event Action<SfxAudioSource> OnDisabled;
    public static event Action<SfxAudioSource> OnEndPlay;

    private Transform initialParent;
    private Transform cachedTransform;

    private AudioSource cachedAudioSource;

    public AudioClip CurrentAudioClip {
        get { return cachedAudioSource.clip; }
    }

    #endregion

    #region Unity lifecycle

    private void Awake() {
        cachedAudioSource = GetComponent<AudioSource>();
        cachedTransform = transform;
    }

    private void OnDisable() {
        StopCoroutine("ProcessPlaying");
        cachedAudioSource.Stop();
        cachedAudioSource.clip = null;

        EventActionUtil.FireEvent(OnDisabled, this);
    }

    #endregion

    #region Public methods

    public void Initialize(Transform parent) {
        initialParent = parent;
        ResetPosition();
    }

    public void ResetPosition() {
        cachedTransform.parent = initialParent;
        cachedTransform.localPosition = Vector3.zero;
    }

    public void Play(AudioClip clip, Vector3 position, Transform parent, float volume, float maxDistance, bool loop) {
        cachedTransform.parent = parent;
        if (parent == initialParent) {
            cachedTransform.position = position;
        } else {
            cachedTransform.localPosition = Vector3.zero;
        }

        cachedAudioSource.clip = clip;
        cachedAudioSource.volume = volume;
        cachedAudioSource.maxDistance = maxDistance;
        cachedAudioSource.loop = loop;
        cachedAudioSource.Play();

        if (!loop) {
            StartCoroutine("ProcessPlaying");
        }
    }

    public void Stop() {
        StopCoroutine("ProcessPlaying");
        cachedAudioSource.Stop();
        cachedAudioSource.clip = null;

        EventActionUtil.FireEvent(OnEndPlay, this);
    }

    #endregion

    #region Private methods

    private IEnumerator ProcessPlaying() {
        float length = cachedAudioSource.clip.length;
        yield return new WaitForSeconds(length);

        cachedAudioSource.clip = null;
        EventActionUtil.FireEvent(OnEndPlay, this);
    }

    #endregion
}