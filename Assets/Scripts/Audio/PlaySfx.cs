﻿using UnityEngine;

public class PlaySfx : MonoBehaviour {
    [SerializeField] private string audioKey;
    [SerializeField] private float maxDistance;
    [SerializeField] [Range(0f, 1f)] private float volume = 1f;
    [SerializeField] private bool usePosition;
    [SerializeField] private bool useTransform;
    [SerializeField] private bool loop;

    private Transform cachedTransform;

    private Transform CachedTransform {
        get { return cachedTransform ?? (cachedTransform = transform); }
    }

    public void Play() {
        //if (usePosition) {
        //    AudioController.Instance.PlaySfx(audioKey, CachedTransform.position, volume, maxDistance, loop);
        //} else {
            AudioController.Instance.PlaySfx(audioKey, volume);
        //}
    }

    public void Stop() {
        AudioController.Instance.StopSfx(audioKey);
    }
}