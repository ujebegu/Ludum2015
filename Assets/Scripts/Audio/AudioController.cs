﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : Singleton<AudioController> {
    #region Variables

    private const string AMBIENT_FOLDER_PATH = "Audio/Ambient/";
    private const string AMBIENT_VOLUME_KEY = "VolumeMusic";
    private const string SFX_FOLDER_PATH = "Audio/Sfx/";
    private const string SFX_VOLUME_KEY = "VolumeEffect";
    private const float FADE_TIME = 1f;
    private const float DEFAULT_SFX_AUDIO_SOURCE_MAX_DISTANCE = 500f;
    private const float DEFAULT_VOLUME_FACTOR = 1f;

    [SerializeField] private AudioSource ambientAudioSource;
    [SerializeField] private SfxAudioSource sfxAudioSourcePrefab;

    private string currentAmbientPath;
    private AudioClip currentAmbientClip;
    private bool isAmbientPlayed;
    private Dictionary<string, AudioClip> cachedAudioClips = new Dictionary<string, AudioClip>();
    private List<SfxAudioSource> availableSfxSources = new List<SfxAudioSource>();
    private List<SfxAudioSource> usedSfxSources = new List<SfxAudioSource>();
    private Transform cachedTransform;

    private Transform CachedTransform {
        get { return cachedTransform ?? (cachedTransform = transform); }
    }

    public float AmbientVolume {
        get { return PlayerPrefs.GetFloat(AMBIENT_VOLUME_KEY, 0.3f); }
    }

    private float SfxVolume {
        get { return PlayerPrefs.GetFloat(SFX_VOLUME_KEY, 1f); }
    }

    public float CurrentAmbientVolume { get; private set; }

    #endregion

    #region Unity lifecycle

    private void Start() {
        StartCoroutine(PlayAmbient());
    }

    private void OnEnable() {
        SfxAudioSource.OnDisabled += SfxAudioSourceOnDisabled;
        SfxAudioSource.OnEndPlay += SfxAudioSourceOnEndPlay;
    }

    private void OnDisable() {
        SfxAudioSource.OnDisabled -= SfxAudioSourceOnDisabled;
        SfxAudioSource.OnEndPlay -= SfxAudioSourceOnEndPlay;
    }

    private void OnLevelWasLoaded(int level) {
        Clear();
        StartCoroutine(PlayAmbient());
    }

    #endregion

    #region Event handlers

    private void SfxAudioSourceOnEndPlay(SfxAudioSource source) {
        usedSfxSources.Remove(source);
        availableSfxSources.Add(source);

        source.ResetPosition();
    }

    private void SfxAudioSourceOnDisabled(SfxAudioSource source) {
        StartCoroutine(ResetSfxAudioSource(source));
    }

    private void SettingMenuOnAmbientVolumeChanged(float ambientVolume) {
        if (!isAmbientPlayed) {
            ambientAudioSource.volume = ambientVolume;
        }
    }

    #endregion

    #region Public methods

    public void MuteAmbient() {
        if (AmbientVolume > float.Epsilon) {
            StartCoroutine(MuteCoroutine(ambientAudioSource.volume, 0.1f));

            CurrentAmbientVolume = 0.1f;
        }
    }

    public void UnMuteAmbient() {
        if (AmbientVolume > float.Epsilon) {
            StartCoroutine(MuteCoroutine(ambientAudioSource.volume, 0.3f));

            CurrentAmbientVolume = 0.3f;
        }
    }

    private IEnumerator MuteCoroutine(float from, float to) {
        float timer = 0f;
        while (timer < 1f) {
            timer += Time.deltaTime;

            ambientAudioSource.volume = Mathf.Lerp(from, to, timer/1f);

            yield return null;
        }
    }
 
    public void UpdateVolume() {
        if (AmbientVolume > float.Epsilon) {
            ambientAudioSource.volume = CurrentAmbientVolume;
        } else {
            ambientAudioSource.volume = AmbientVolume;
        }
    }

    public void PlaySfx(string audioKey, bool loop) {
        PlaySfx(audioKey, Vector3.zero, CachedTransform, DEFAULT_VOLUME_FACTOR, DEFAULT_SFX_AUDIO_SOURCE_MAX_DISTANCE,
            loop);
    }

    public void PlaySfx(string audioKey, Vector3 position, float maxDistance) {
        PlaySfx(audioKey, position, CachedTransform, DEFAULT_VOLUME_FACTOR, maxDistance, false);
    }

    public void PlaySfx(string audioKey, Transform parent, float maxDistance) {
        PlaySfx(audioKey, Vector3.zero, parent, DEFAULT_VOLUME_FACTOR, maxDistance, false);
    }

    public void PlaySfx(string audioKey, Transform parent, float volume, float maxDistance, bool loop) {
        PlaySfx(audioKey, Vector3.zero, parent, volume, maxDistance, loop);
    }

    public void PlaySfx(string audioKey) {
        PlaySfx(audioKey, Vector3.zero);
    }

    public void PlaySfx(string audioKey, float volume) {
        PlaySfx(audioKey, Vector3.zero, CachedTransform, volume);
    }

    public void PlaySfx(string audioKey, Vector3 position) {
        PlaySfx(audioKey, position, CachedTransform, DEFAULT_SFX_AUDIO_SOURCE_MAX_DISTANCE);
    }

    public void PlaySfx(string audioKey, Vector3 position, float volume, float maxDistance, bool loop) {
        PlaySfx(audioKey, position, CachedTransform, volume, maxDistance, loop);
    }

    public void PlaySfx(string audioKey, Vector3 position, Transform parent, float maxDistance) {
        PlaySfx(audioKey, position, parent, DEFAULT_VOLUME_FACTOR, maxDistance, false);
    }

    public void PlaySfx(string audioKey, Vector3 position, Transform parent, float volume, float maxDistance, bool loop) {
        string audioPath = SFX_FOLDER_PATH + audioKey;
        AudioClip targetAudioClip = GetOrLoadAudioClip(audioPath);
        if (targetAudioClip != null) {
            SfxAudioSource targetSfxAudioSource = GetSfxAudioSource();

            if (SfxVolume > float.Epsilon) {
                targetSfxAudioSource.Play(targetAudioClip, position, parent, volume * SfxVolume, maxDistance, loop);
            }
        } else {
            Debug.LogWarning("There is no audio clip at path -> " + audioPath);
        }
    }

    public void StopSfx(string audioKey) {
        string audioPath = SFX_FOLDER_PATH + audioKey;
        if (cachedAudioClips.ContainsKey(audioPath)) {
            AudioClip targetAudioClip = cachedAudioClips[audioPath];
            foreach (var sfxAudioSource in usedSfxSources.ToArray()) {
                if (sfxAudioSource != null && sfxAudioSource.CurrentAudioClip == targetAudioClip) {
                    sfxAudioSource.Stop();
                }
            }
        }
    }

    #endregion

    #region Private methods

    private IEnumerator ResetSfxAudioSource(SfxAudioSource source) {
        yield return new WaitForEndOfFrame();

        if (source != null) {
            source.gameObject.SetActive(true);
            source.ResetPosition();

            usedSfxSources.Remove(source);
            availableSfxSources.Add(source);
        } else {
            var sources = usedSfxSources.ToArray();
            for (int i = 0; i < sources.Length; i++) {
                if (sources[i] == null) {
                    usedSfxSources.RemoveAt(i);
                }
            }
        }
    }

    private AudioClip GetOrLoadAudioClip(string audioPath) {
        AudioClip targetAudioClip;
        if (cachedAudioClips.ContainsKey(audioPath)) {
            targetAudioClip = cachedAudioClips[audioPath];
        } else {
            targetAudioClip = Resources.Load(audioPath, typeof (AudioClip)) as AudioClip;
            cachedAudioClips.Add(audioPath, targetAudioClip);
        }

        return targetAudioClip;
    }

    private SfxAudioSource GetSfxAudioSource() {
        if (availableSfxSources.Count > 0) {
            var result = availableSfxSources[0];
            availableSfxSources.Remove(result);
            usedSfxSources.Add(result);
            return result;
        }

        SfxAudioSource newSfx = Instantiate(sfxAudioSourcePrefab);
        if (newSfx != null) {
            newSfx.Initialize(CachedTransform);
            usedSfxSources.Add(newSfx);
        }

        return newSfx;
    }

    private void Clear() {
        foreach (var key in cachedAudioClips.Keys) {
            Resources.UnloadAsset(cachedAudioClips[key]);
        }

        cachedAudioClips.Clear();
    }

    private IEnumerator PlayAmbient() {
        if (!isAmbientPlayed) {
            isAmbientPlayed = true;

            string nextAmbientPath = AMBIENT_FOLDER_PATH + "Ambient";
            if (!string.IsNullOrEmpty(nextAmbientPath) && !string.Equals(nextAmbientPath, currentAmbientPath)) {
                AudioClip nextAmbientClip = Resources.Load(nextAmbientPath, typeof(AudioClip)) as AudioClip;
                if (nextAmbientClip != null) {
                    if (ambientAudioSource.isPlaying) {
                        yield return StartCoroutine(FadeAmbient(AmbientVolume, 0f));
                        ambientAudioSource.Stop();
                    }

                    ambientAudioSource.clip = nextAmbientClip;
                    ambientAudioSource.Play();

                    //yield return StartCoroutine(FadeAmbient(0f, AmbientVolume));

                    if (AmbientVolume > float.Epsilon) {
                        ambientAudioSource.volume = 0.1f;
                        CurrentAmbientVolume = 0.1f;
                    }

                    currentAmbientPath = nextAmbientPath;
                    if (currentAmbientClip != null) {
                        Resources.UnloadAsset(currentAmbientClip);
                    }

                    currentAmbientClip = nextAmbientClip;
                }
            }

            isAmbientPlayed = false;
        }
    }

    private IEnumerator FadeAmbient(float from, float to) {
        ambientAudioSource.volume = from;

        float timer = 0f;
        while (timer < FADE_TIME) {

            ambientAudioSource.volume = Mathf.Lerp(from, to, timer / FADE_TIME);

            timer += Time.deltaTime;

            yield return null;
        }

        ambientAudioSource.volume = to;
    }

    #endregion
}