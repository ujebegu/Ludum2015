﻿using System.Collections.Generic;
using UnityEngine;

public sealed class PoolManager<V> where V : IPoolObject {
    #region Variables

    private int maxInstanceCount;
    private List<V> objects;

    private bool CanPush {
        get { return objects.Count + 1 < maxInstanceCount; }
    }

    public UnityPoolObject Prefab { get; private set; }

    public Transform Root { get; private set; }

    #endregion

    #region Public methods

    public PoolManager(PoolObjectInfo poolObjectInfo, Transform root) {
        maxInstanceCount = poolObjectInfo.MaxInstanceCount;
        Prefab = poolObjectInfo.Prefab;
        Root = root;
        objects = new List<V>();
    }

    public bool Push(V value) {
        bool result;

        if (CanPush) {
            value.OnPush();
            objects.Add(value);
            result = true;
        } else {
            value.FailedPush();
            result = false;
        }

        return result;
    }

    public T Pop<T>() where T : V {
        T result = default(T);
        if (objects.Count > 0) {
            result = (T) objects[0];
            objects.RemoveAt(0);
            result.Create();
        }

        return result;
    }

    #endregion
}