﻿public interface IPoolObject {
    PoolType PoolType { get; }
    void Create();
    void OnPush();
    void FailedPush();
}