﻿using System;

[Serializable]
public struct PoolObjectInfo {
    public UnityPoolObject Prefab;
    public int MaxInstanceCount;
}