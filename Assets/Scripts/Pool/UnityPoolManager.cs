﻿using System.Collections.Generic;
using UnityEngine;

public sealed class UnityPoolManager : DestroyableSingleton<UnityPoolManager> {
    #region Variables

    [SerializeField] private PoolObjectInfo[] poolsInfo;

    private Transform cachedTransform;

    private Dictionary<PoolType, PoolManager<UnityPoolObject>> pools =
        new Dictionary<PoolType, PoolManager<UnityPoolObject>>();

    #endregion

    #region Unity lifecycle

    protected override void Awake() {
        base.Awake();

        cachedTransform = transform;

        foreach (var poolObjectInfo in poolsInfo) {
            if (poolObjectInfo.Prefab != null && !pools.ContainsKey(poolObjectInfo.Prefab.PoolType)) {
                pools.Add(poolObjectInfo.Prefab.PoolType,
                    new PoolManager<UnityPoolObject>(poolObjectInfo,
                        CreateRoot(poolObjectInfo.Prefab.PoolType.ToString())));
            }
        }
    }

    #endregion

    #region Public methods

    public bool Push(UnityPoolObject poolObject) {
        return pools.ContainsKey(poolObject.PoolType) && pools[poolObject.PoolType].Push(poolObject);
    }

    public T Pop<T>(PoolType poolType) where T : UnityPoolObject {
        return Pop<T>(poolType, Vector3.zero, Quaternion.identity);
    }

    public T Pop<T>(PoolType poolType, Vector3 position, Quaternion rotation) where T : UnityPoolObject {
        if (pools.ContainsKey(poolType)) {
            T result = pools[poolType].Pop<T>();
            if (result == null) {
                result = CreateObject<T>(pools[poolType].Prefab, position, rotation, pools[poolType].Root);
            } else {
                result.SetTransform(position, rotation);
            }
            return result;
        }

        return null;
    }

    #endregion

    #region Private methods

    private Transform CreateRoot(string rootName) {
        GameObject root = new GameObject(rootName);

        Transform rootTransform = root.transform;
        rootTransform.parent = cachedTransform;
        rootTransform.localPosition = Vector3.zero;
        rootTransform.localPosition = Vector3.zero;
        rootTransform.localScale = Vector3.one;

        return rootTransform;
    }

    private T CreateObject<T>(UnityPoolObject prefab, Vector3 position, Quaternion rotation, Transform parent)
        where T : UnityPoolObject {
        GameObject go = Instantiate(prefab.gameObject);
        T result = go.GetComponent<T>();

        result.CachedTransform.parent = parent;
        result.SetTransform(position, rotation);

        return result;
    }

    #endregion
}