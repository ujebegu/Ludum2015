﻿using UnityEngine;

public class UnityPoolObject : MonoBehaviour, IPoolObject {
    #region Variables

    [SerializeField] private PoolType poolType;

    private bool InPool {
        set { gameObject.SetActive(!value); }
    }

    public Transform CachedTransform { get; private set; }

    #endregion

    #region Unity lifecycle

    protected virtual void Awake() {
        CachedTransform = transform;
    }

    #endregion

    #region IPoolObject

    public PoolType PoolType {
        get { return poolType; }
    }

    public virtual void Create() {
        InPool = false;
    }

    public virtual void OnPush() {
        InPool = true;
    }

    public void FailedPush() {
        Destroy(gameObject);
    }

    #endregion

    #region Public methods

    public virtual void SetTransform(Vector3 position, Quaternion rotation) {
        CachedTransform.position = position;
        CachedTransform.rotation = rotation;
    }

    public virtual void Push() {
        UnityPoolManager.Instance.Push(this);
    }

    #endregion
}