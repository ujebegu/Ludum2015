﻿using UnityEditor;
using UnityEngine;

public class CreateScriptableObjectUtility : Editor {
    [MenuItem("Tools/CreateScriptableObjectFromScript", false, 10000)]
    public static void CreateManager() {
        ScriptableObject asset = CreateInstance(Selection.activeObject.name);
        AssetDatabase.CreateAsset(asset, string.Format("Assets/{0}.asset", Selection.activeObject.name));
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }

    [MenuItem("Tools/CreateScriptableObjectFromScript", true, 10000)]
    public static bool CreateManagerValidate() {
        return true;
    }
}