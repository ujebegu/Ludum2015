﻿using UnityEditor;
using UnityEngine;

public class ClearPrefs : Editor {
    [MenuItem("Tools/ClearPrefs", false, 10000)]
    public static void CreateManager() {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
    }
}